# Bastille nginx

---

#### Arguments


| supported | format             | example                                        |
|:---------:|:------------------:|:-----------------------------------------------|
| ARG       | name=value         | ARG VERSION=13                                 |
| LIMITS    | resource value     | memoryuser 2G                                  |
| INCLUDE   | template path/url  | https://TEMPLATE_URL or username/base-template |
| PRE       | /bin/sh command    | mkdir -p /usr/local/path                       |
| FSTAB     | fstab syntax       | /host/path container/path nullfs ro 0 0        |
| PKG       | port/pkg name(s)   | vim curl wget p7zip bash bash-completion       |
| OVERLAY   | paths              | etc usr                                        |
| SYSRC     | sysrc command(s)   | nginx_enable=YES                               |
| SERVICE   | service command    | nginx restart                                  |
| CMD       | /bin/bash command  | chsh -s bash root                              |
| RENDER    | paths              | /usr/local/etc nginx                           |
| RDR       | protocol port port | tcp 80 80                                      |


Note: SYSRC requires NO quotes or that quotes (") be escaped. ie; \")

Any name provided in the ARG file can be used as a variable in the other hooks. For example, name=value in the ARG file will cause instances of ${name} to be replaced with value. The RENDER hook can be used to specify existing files or directories inside the jail whose contents should have the variables replaced. Values can be specified either through the command line when applying the template or as a default in the ARG file.

In addition to supporting template hooks, Bastille supports overlaying files into the container. This is done by placing the files in their full path, using the template directory as "/".

An example here may help. Think of /usr/local/bastille/templates/username/base, our example template, as the root of our filesystem overlay. If you create an etc/hosts or etc/resolv.conf inside the base template directory, these can be overlayed into your container.

Note: due to the way FreeBSD segregates user-space, the majority of your overlayed template files will be in usr/local. The few general exceptions are the etc/hosts, etc/resolv.conf, and etc/rc.conf.local.

After populating usr/local/ with custom config files that your container will use, be sure to include usr in the template OVERLAY definition. eg;

---

#### Network tips

##### Tip #1

Ports and destinations can be defined as lists. eg;

```bash
rdr pass inet proto tcp from any to any port {80, 443} -> {10.17.89.45, 10.17.89.46, 10.17.89.47, 10.17.89.48}
```

##### Tip #2

Ports can redirect to other ports. eg;

```bash
rdr pass inet proto tcp from any to any port 8080 -> 10.17.89.5 port 80
rdr pass inet proto tcp from any to any port 8081 -> 10.17.89.5 port 8080
rdr pass inet proto tcp from any to any port 8181 -> 10.17.89.5 port 443
```

##### Tip #2

Don't worry too much about IP assignments.

Initially I spent time worrying about what IP addresses to assign. In the end I've come to the conclusion that it really doesn't matter. Pick any private address and be done with it. These are all isolated networks. In the end, what matters is you can map host:port to container:port reliably, and we can.

---

#### How to use this

```bash
[root@machine ~/ ] #: bastille template [jail] https://gitlab.com/fredericosales/bastille-nginx
```

---

#### Change some arguments

```bash
[root@machine ~/ ] #: bastille template [jail] https://gitlab.com/fredericosales/bastille-nginx
```

---